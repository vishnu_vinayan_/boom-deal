import React, { useState } from "react";
import Layout from "../core/Layout";
import {signup} from '../auth';
import { Link } from 'react-router-dom'

const Signup = () => {
  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
    error: "",
    success: false,
  });

  const { name, email, password, error, success } = values;

  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });
  };

  

  const clickSubmit = (event) => {
    event.preventDefault();
    setValues({ ...values, error: false});
    signup({ name, email, password }).then((data) => {
        console.log('##ddd', data)
      if (data.err) {
        setValues({ ...values, error: data.err, success: false });
      } else {
        setValues({
          ...values,
          name: "",
          email: "",
          password: "",
          err: "",
          success: true,
        });
      }
    });
  };

  const showError = () => (
    <div
      className="alert alert-danger"
      style={{ display: error ? "" : "none" }}
    >
      {error}
    </div>
  );

  const showSuccess = () => (
    <div
      className="alert alert-success"
      style={{ display: success ? "" : "none" }}
    >
      New account has been created. Please <Link to='/signin'>Sign in</Link>
    </div>
  );

  const signupForm = () => (
    <form>
      <div className="form-group">
        <label className="text-muted">Name</label>
        <input
          onChange={handleChange("name")}
          type="text"
          className="form-control"
          value={name}
        ></input>
      </div>
      <div className="form-group">
        <label className="text-muted">Email</label>
        <input
          onChange={handleChange("email")}
          type="email"
          className="form-control"
          value={email}
        ></input>
      </div>
      <div className="form-group">
        <label className="text-muted">Password</label>
        <input
          onChange={handleChange("password")}
          type="password"
          className="form-control"
          value={password}
        ></input>
      </div>
      <button onClick={clickSubmit} className="btn btn-primary">
        Submit
      </button>
    </form>
  );

  return (
    <Layout
      title="Sign up"
      description="Node React Sign up Page"
      className="container col-md-8 offset-md-2"
    >
      {showError()}
      {showSuccess()}
      {signupForm()}
    </Layout>
  );
};

export default Signup;
